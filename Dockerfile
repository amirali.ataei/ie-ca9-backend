##FROM openjdk:11-jre-slim as build
##WORKDIR /workspace/app
##
##COPY mvnw .
###COPY .mvn .mvn
##COPY pom.xml .
##COPY src src
##
###RUN ./mvnw install -DskipTests
##RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)
##
##FROM openjdk:11-jre-slim
##VOLUME /tmp
##ARG DEPENDENCY=/workspace/app/target/dependency
##COPY --from=build ${DEPENDENCY}/BOOT-INF/lib /app/lib
##COPY --from=build ${DEPENDENCY}/META-INF /app/META-INF
##COPY --from=build ${DEPENDENCY}/BOOT-INF/classes /app
##ENTRYPOINT ["java","-cp","app:app/lib/*","hello.Application"]
#
FROM maven:3.6.0-jdk-11-slim AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package

#
# Package stage
#
FROM openjdk:11-jre-slim
COPY --from=build /home/app/target/CA6-0.0.1-SNAPSHOT.jar /usr/local/lib/demo.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/local/lib/demo.jar"]

#FROM java:8-jdk-alpine
#
#COPY ./target/CA6-0.0.1-SNAPSHOT.jar /usr/app/
#
#WORKDIR /usr/app
#
#RUN sh -c 'touch CA6-0.0.1-SNAPSHOT.jar'
#
#ENTRYPOINT ["java","-jar","CA6-0.0.1-SNAPSHOT.jar"]
