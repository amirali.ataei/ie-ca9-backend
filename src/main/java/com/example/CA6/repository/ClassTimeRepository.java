package com.example.CA6.repository;

import com.example.CA6.service.Course;
import org.json.simple.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ClassTimeRepository extends Repository<Course, String>{
    private static final String TABLE_NAME = "ClassTime";
    private static ClassTimeRepository instance;

    public static ClassTimeRepository getInstance() {
        if (instance == null) {
            try {
                instance = new ClassTimeRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in ClassTimeRepository.create query.");
            }
        }
        return instance;
    }

    private ClassTimeRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement createTableStatement = con.prepareStatement(
                String.format("CREATE TABLE IF NOT EXISTS %s (\n" +
                        "  `code` char(225) NOT NULL,\n" +
                        "  `classCode` char(225) NOT NULL,\n" +
                        "  `day1` char(225) DEFAULT NULL,\n" +
                        "  `day2` char(225) DEFAULT NULL,\n" +
                        "  `day3` char(225) DEFAULT NULL,\n" +
                        "  `time` char(225) NOT NULL,\n" +
                        "  PRIMARY KEY (`code`,`classCode`),\n" +
                        "  CONSTRAINT `classCode` FOREIGN KEY (`code`, `classCode`) REFERENCES `CourseGroup` (`code`, `classCode`) ON DELETE CASCADE\n" +
                        ");", TABLE_NAME)
        );
        createTableStatement.executeUpdate();
        createTableStatement.close();
        con.close();
    }


    @Override
    protected String getFindByIdStatement() {
        return null;
    }

    @Override
    protected void fillFindByIdValues(PreparedStatement st, String id) throws SQLException {

    }

    @Override
    protected String getInsertStatement() {
        return String.format("INSERT INTO %s(code, classCode, day1, day2, day3, time) Select ?, ?, ?, ?, ?, ? where not exists(Select * from %s where code = ? and classCode = ?);", TABLE_NAME, TABLE_NAME);
    }

    @Override
    protected void fillInsertValues(PreparedStatement st, Course data) throws SQLException {
        st.setString(1, data.getCode());
        st.setString(2, data.getClassCode());

        JSONObject classTime = data.getClassTime();
        ArrayList<String> days = (ArrayList<String>) classTime.get("days");
        String[] day = new String[3];
        day[0] = "";
        day[1] = "";
        day[2] = "";
        for(int i = 0; i < days.size(); i ++) {
            day[i] = days.get(i);
        }
        st.setString(3, day[0]);
        st.setString(4, day[1]);
        st.setString(5, day[2]);
        String time = (String) classTime.get("time");
        st.setString(6, time);
        st.setString(7, data.getCode());
        st.setString(8, data.getClassCode());
    }

    @Override
    protected String getFindAllStatement() {
        return null;
    }

    @Override
    protected void fillFindBySearchTypeValues(PreparedStatement st, String search, String type) throws SQLException {

    }

    @Override
    protected Course convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        return null;
    }

    @Override
    protected String getFindBySearchType() {
        return null;
    }

    @Override
    protected ArrayList<Course> convertResultSetToDomainModelList(ResultSet rs) throws SQLException {
        return null;
    }

    @Override
    protected void fillInsertByIdValues(PreparedStatement st, Course obj, String id) throws SQLException {
    }

    @Override
    protected String getFindBySearch() {
        return null;
    }

    @Override
    protected void fillFindBySearchValues(PreparedStatement st, String search) throws SQLException {

    }

    @Override
    protected void fillFindByTermValues(PreparedStatement st, String id) {

    }

    @Override
    protected String getFindByTermStatement() {
        return null;
    }

    @Override
    protected void fillFindByEmailValues(PreparedStatement st, String email) throws SQLException {

    }

    @Override
    protected String getFindByEmailStatement() {
        return null;
    }
}
