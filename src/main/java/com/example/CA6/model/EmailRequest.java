package com.example.CA6.model;

public class EmailRequest {

        private static final long serialVersionUID = 5926468583005150707L;
        private String email;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

}
