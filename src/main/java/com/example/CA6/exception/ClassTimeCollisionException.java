package com.example.CA6.exception;

public class ClassTimeCollisionException extends Exception{
    private String code1;
    private String code2;

    public ClassTimeCollisionException(String code, String _code) {
        this.code1 = code;
        this.code2 = _code;
    }
    public String toString(){
        return "ClassTimeCollisionError" + this.code1 + this.code2;
    }
}
