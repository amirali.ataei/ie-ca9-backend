package com.example.CA6.service;

import static com.example.CA6.util.Tools.getCourse;

public class Grade{
    private String code;
    private double grade;
    private int term;

    public Grade(String _code, double _grade, int _term) {
        code = _code;
        grade = _grade;
        term = _term;
    }

    public String getCode() {
        return code;
    }

    public double getGrade() {
        return grade;
    }

    public int getTerm() {
        return term;
    }

    public int getUnits() {
        int units = 0;
        try {
            units = getCourse(code, "1").getUnits();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return units;
    }

    public String getName() {
        String name = "";
        try {
            name = getCourse(code, "1").getName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return name;
    }
}
