package com.example.CA6.controller;

import com.example.CA6.model.Cards;
import com.example.CA6.repository.StudentRepository;
import com.example.CA6.service.Student;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import static com.example.CA6.util.Tools.getStudent;

@RestController
@CrossOrigin
@RequestMapping(value = "/reports", produces = MediaType.APPLICATION_JSON_VALUE)
public class ReportsController {
    private final StudentRepository studentRepository = StudentRepository.getInstance();
    @GetMapping(value = "")
    public Cards getReports(@RequestAttribute(name = "User") Student student) {
        Cards cards = new Cards();
        System.out.println(student.getStudentId());
        try {
            cards = new Cards(student);
            cards.setStatusCode(202);
        } catch (Exception e) {
            cards.setStatusCode(405);
            e.printStackTrace();
        }
        return cards;
    }
}
