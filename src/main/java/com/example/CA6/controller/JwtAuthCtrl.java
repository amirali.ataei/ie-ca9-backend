package com.example.CA6.controller;

import com.example.CA6.model.JwtRequest;
import com.example.CA6.model.JwtResponse;
import com.example.CA6.repository.StudentRepository;
import com.example.CA6.service.Student;
import com.example.CA6.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.security.authentication.DisabledException;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.ArrayList;

import static com.example.CA6.util.Tools.studentId;

@RestController
@CrossOrigin
@RequestMapping(value = "/authentication", produces = MediaType.APPLICATION_JSON_VALUE)
public class JwtAuthCtrl {

    @Autowired
    private JwtUtil jwtUtil;

    private final StudentRepository studentRepository = StudentRepository.getInstance();

    @PostMapping(value = "")
    public JwtResponse createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) {
        String email = authenticationRequest.getEmail();
        String password = authenticationRequest.getPassword();
        System.out.println("Bye");
        Student student = null;
        try {
            ArrayList<Student> s = (ArrayList<Student>) studentRepository.findBySearchType(email, password);
            if(s.size() == 0)
                return new JwtResponse("");
//                return (ResponseEntity<?>) ResponseEntity.status(403);
            else
                student = s.get(0);
        } catch (SQLException throwables) {
        }

        studentId = student.getStudentId();
        final String token = jwtUtil.generateToken(student);
        System.out.println("HI");
        return new JwtResponse(token);
//        return ResponseEntity.ok(new JwtResponse(token));
    }
}
