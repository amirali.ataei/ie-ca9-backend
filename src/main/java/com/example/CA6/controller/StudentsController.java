package com.example.CA6.controller;

import com.example.CA6.model.IdPack;
import com.example.CA6.model.JwtResponse;
import com.example.CA6.model.Std;
import com.example.CA6.repository.StudentRepository;
import com.example.CA6.service.Student;
import com.example.CA6.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;

import static com.example.CA6.util.Tools.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/students", produces = MediaType.APPLICATION_JSON_VALUE)
public class StudentsController {
    private final StudentRepository studentRepository = StudentRepository.getInstance();

    @Autowired
    private JwtUtil jwtUtil;

    @GetMapping("")
    public IdPack getGetStudent(@RequestParam(value = "email") String email, @RequestParam(value = "password") String password) {
        IdPack idPack = new IdPack();
        Student student = null;
        try {
            student = studentRepository.findBySearchType(email, password).get(0);
            idPack.setId(student.getStudentId());
            studentId = student.getStudentId();
        } catch (Exception e) {
            idPack.setId("");
        }

        return idPack;
    }

    @PostMapping("")
    public ResponseEntity<?> postStudent(@RequestBody Std std) {
        IdPack idPack = new IdPack();
        Student student = new Student(
                std.getStdId(),
                std.getName(),
                std.getEmail(),
                std.getPassword(),
                std.getSecondName(),
                std.getBirthDate(),
                std.getField(),
                std.getFaculty(),
                std.getLevel(),
                std.getStatus(),
                std.getImg());

        try {
            studentRepository.insert(student);
            studentId = std.getStdId();
            final String token = jwtUtil.generateToken(student);
            return ResponseEntity.ok(new JwtResponse(token));
        } catch (SQLException throwables) {
            idPack.setId("");
            return (ResponseEntity<?>) ResponseEntity.status(403);
        }

    }
}
